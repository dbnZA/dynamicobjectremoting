// <copyright file="FinaliseTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

public class FinaliseTests : DomainTester
{
    private static bool hasFinalised;

    [Test]
    public void TestFinalise()
    {
        this.CreateAndDiscard();

        // Trigger finalizer for <see cref="DynamicRemoteObject"/> instance.
        GC.Collect();
        GC.WaitForPendingFinalizers();

        // Trigger finalizer for underlying <see cref="Finalise"/> instance.
        GC.Collect();
        GC.WaitForPendingFinalizers();

        Assert.IsTrue(hasFinalised);
    }

    private void CreateAndDiscard()
        => _ = this.RemoteDomain.Root.TestDynamicObjectRemoting.FinaliseTests.Finalise();

    public class Finalise
    {
        ~Finalise()
            => hasFinalised = true;
    }
}
