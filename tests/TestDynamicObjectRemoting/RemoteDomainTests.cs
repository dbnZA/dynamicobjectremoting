// <copyright file="RemoteDomainTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

using System.Net.Sockets;
using DynamicObjectRemoting;
using Microsoft.CSharp.RuntimeBinder;

public class RemoteDomainTests : DomainTester
{
    [Test]
    public void CoverRethrowSocketException()
        => Assert.ThrowsAsync<SocketException>(() => TcpRemoteDomain.ConnectAsync("localhost", 0));

    [Test]
    public void TestMissingType()
        => Assert.Throws<RuntimeBinderException>(() => _ = this.RemoteDomain.Root.FooBar);
}
