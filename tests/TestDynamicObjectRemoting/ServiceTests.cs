// <copyright file="ServiceTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

using System.Net.Sockets;
using DynamicObjectRemoting;

public class ServiceTests : DomainTester
{
    [Test]
    public void CoverRethrowSocketException()
        => Assert.Throws<SocketException>(() => TcpService.ListenAsync(this.EndPoint, default));

    [Test]
    public async Task CoverReuseWeakRefAsync()
    {
        (await TcpRemoteDomain.ConnectAsync(this.EndPoint)).Dispose();
        GC.Collect();
        (await TcpRemoteDomain.ConnectAsync(this.EndPoint)).Dispose();
    }
}
