// <copyright file="PropertyTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

using DynamicObjectRemoting;
using Microsoft.CSharp.RuntimeBinder;

public class PropertyTests : DomainTester
{
    [Test]
    public void TestReadWrite()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        Assert.DoesNotThrow(() => obj.Int32 = 1);
        Assert.That((int)obj.Int32, Is.EqualTo(1));
    }

    [Test]
    public void TestReadOnlyWriteOnly()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        Assert.DoesNotThrow(() => obj.IntWriteOnly = 1);
        Assert.Throws<RuntimeBinderException>(() => _ = obj.IntWriteOnly);
        Assert.Throws<RuntimeBinderException>(() => obj.IntReadOnly = 1);
        Assert.That((int)obj.IntReadOnly, Is.EqualTo(1));
    }

    [Test]
    public void TestException()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        var ex = Assert.Throws<RemoteDomainException>(() => _ = obj.Exception);
        Assert.That(ex!.RemoteException.Message, Is.EqualTo("1"));
    }

    [Test]
    public void TestNotFound()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        var ex = Assert.Throws<RuntimeBinderException>(() => _ = obj.NotFound);
        Assert.That(ex!.Message, Is.EqualTo("No definition for member 'NotFound'"));
        ex = Assert.Throws<RuntimeBinderException>(() => obj.NotFound = 1);
        Assert.That(ex!.Message, Is.EqualTo("No definition for setting member 'NotFound'"));
    }

    [Test]
    public void TestBadSet()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        var ex = Assert.Throws<RemoteDomainException>(() => _ = obj.Int32 = 1u);
        Assert.That(ex!.Message, Is.EqualTo("Object of type 'System.UInt32' cannot be converted to type 'System.Int32'."));
    }

    [Test]
    public void TestNull()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        Assert.That((object?)obj.Null, Is.Null);
    }

    [Test]
    public void TestBool()
        => this.TestType(true, obj => obj.Bool, (obj, x) => obj.Bool = x);

    [Test]
    public void TestByte()
        => this.TestType<byte>(0b1, obj => obj.Byte, (obj, x) => obj.Byte = x);

    [Test]
    public void TestDecimal()
        => this.TestType(1m, obj => obj.Decimal, (obj, x) => obj.Decimal = x);

    [Test]
    public void TestDouble()
        => this.TestType(1d, obj => obj.Double, (obj, x) => obj.Double = x);

    [Test]
    public void TestFloat()
        => this.TestType(1f, obj => obj.Float, (obj, x) => obj.Float = x);

    [Test]
    public void TestInt32()
        => this.TestType(1, obj => obj.Int32, (obj, x) => obj.Int32 = x);

    [Test]
    public void TestInt64()
        => this.TestType(1L, obj => obj.Int64, (obj, x) => obj.Int64 = x);

    [Test]
    public void TestNInt()
        => this.TestType<nint>(1, obj => obj.NInt, (obj, x) => obj.NInt = x);

    [Test]
    public void TestNUInt32()
        => this.TestType<nuint>(1, obj => obj.NUInt, (obj, x) => obj.NUInt = x);

    [Test]
    public void TestSByte()
        => this.TestType<sbyte>(0b1, obj => obj.SByte, (obj, x) => obj.SByte = x);

    [Test]
    public void TestInt16()
        => this.TestType<short>(1, obj => obj.Int16, (obj, x) => obj.Int16 = x);

    [Test]
    public void TestString()
        => this.TestType("1", obj => obj.String, (obj, x) => obj.String = x);

    [Test]
    public void TestUInt32()
        => this.TestType(1u, obj => obj.UInt32, (obj, x) => obj.UInt32 = x);

    [Test]
    public void TestUInt64()
        => this.TestType(1ul, obj => obj.UInt64, (obj, x) => obj.UInt64 = x);

    [Test]
    public void TestUInt16()
        => this.TestType<ushort>(1, obj => obj.UInt16, (obj, x) => obj.UInt16 = x);

    private void TestType<T>(T value, Func<dynamic, dynamic> getter, Action<dynamic, T> setter)
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.PropertyTests.Property();
        setter(obj, value);
        Assert.That((T)getter(obj), Is.EqualTo(value));
    }

    public class Property
    {
        private int @int;

        public object Exception => throw new Exception("1");

        public object? Null { get; }

        public bool Bool { get; set; }

        public byte Byte { get; set; }

        public decimal Decimal { get; set; }

        public double Double { get; set; }

        public float Float { get; set; }

        public int Int32 { get; set; }

        public long Int64 { get; set; }

        public nint NInt { get; set; }

        public nuint NUInt { get; set; }

        public sbyte SByte { get; set; }

        public short Int16 { get; set; }

        public string String { get; set; } = null!;

        public uint UInt32 { get; set; }

        public ulong UInt64 { get; set; }

        public ushort UInt16 { get; set; }

        public int IntReadOnly => this.@int;

        public int IntWriteOnly
        {
            set => this.@int = value;
        }
    }
}
