// <copyright file="MethodTester.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

using System.Numerics;
using DynamicObjectRemoting;
using Microsoft.CSharp.RuntimeBinder;

public class MethodTester : DomainTester
{
    private static bool targetCalled;

    [Test]
    public void TestCallMethod()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.MethodTester.Method();
        obj.Target();
        Assert.That(targetCalled, Is.True);
    }

    [Test]
    public void TestMethodNotFound()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.MethodTester.Method();
        var ex = Assert.Throws<RuntimeBinderException>(() => obj.NotFound());
        Assert.That(ex!.Message, Is.EqualTo("No definition for member 'NotFound'"));
    }

    [Test]
    public void TestCallMethodWithArg()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.MethodTester.Method();
        Assert.That(obj.Add(1), Is.EqualTo(2));
    }

    [Test]
    public void TestCallOverloadMethod()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.MethodTester.Method();
        Assert.That(obj.Subtract(1), Is.EqualTo(0));
        Assert.That(obj.Subtract(1L), Is.EqualTo(-1L));
    }

    [Test]
    public void TestGeneric()
    {
#pragma warning disable SA1312
        var Single = this.RemoteDomain.Root.System.Single;
        var Boolean = this.RemoteDomain.Root.System.Boolean;
#pragma warning restore SA1312

        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.MethodTester.Method();
        Assert.That(obj.Subtract[Single](2f), Is.EqualTo(1f));
        Assert.Throws<RemoteDomainException>(() => _ = obj.Subtract[Boolean]);
    }

    public class Method
    {
        public void Target()
            => targetCalled = true;

        public int Add(int arg)
            => arg + 1;

        public int Subtract(int arg)
            => arg - 1;

        public long Subtract(long arg)
            => arg - 2L;

        public T Subtract<T>(T arg)
            where T : INumber<T>
            => arg - T.One;
    }
}
