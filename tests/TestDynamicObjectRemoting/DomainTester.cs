// <copyright file="DomainTester.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

using System.Net;
using DynamicObjectRemoting;

public class DomainTester : IDisposable
{
    private readonly CancellationTokenSource tokenSource = new();

    private Task listener = null!;

    protected TcpRemoteDomain RemoteDomain { get; private set; } = null!;

    protected IPEndPoint EndPoint { get; private set; } = null!;

    [OneTimeSetUp]
    public async Task SetupAsync()
    {
        (this.listener, this.EndPoint) = await TcpService.ListenAsync("localhost", 0, this.tokenSource.Token);
        this.RemoteDomain = await TcpRemoteDomain.ConnectAsync("localhost", this.EndPoint.Port);
    }

    public void Dispose()
    {
        this.RemoteDomain.Dispose();

        this.tokenSource.Cancel();
        this.listener
            .ConfigureAwait(continueOnCapturedContext: false)
            .GetAwaiter()
            .GetResult();
        this.listener.Dispose();
    }
}
