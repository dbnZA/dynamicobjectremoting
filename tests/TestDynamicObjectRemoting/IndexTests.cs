// <copyright file="IndexTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

public class IndexTests : DomainTester
{
    [Test]
    public void TestIndex()
    {
        var obj = this.RemoteDomain.Root.TestDynamicObjectRemoting.IndexTests.Index();
        Assert.That((int)obj[0], Is.EqualTo(1));

        obj[0] = 0;
        Assert.Multiple(() =>
        {
            Assert.That((int)obj[2], Is.EqualTo(1));
            Assert.That((int)obj[3], Is.EqualTo(0));
        });
        obj[0] = 3;
        Assert.Multiple(() =>
        {
            Assert.That((int)obj[2], Is.EqualTo(0));
            Assert.That((int)obj[3], Is.EqualTo(1));
        });
    }

    [Test]
    public void TestGenericIndex()
    {
#pragma warning disable SA1312
        var Int32 = this.RemoteDomain.Root.System.Int32;
        var IEnumerable = this.RemoteDomain.Root.System.Collections.Generic.IEnumerable;
        var List = this.RemoteDomain.Root.System.Collections.Generic.List;
#pragma warning restore SA1312

        Assert.That(this.RemoteDomain.TypeOf(List[Int32]).IsAssignableTo(this.RemoteDomain.TypeOf(IEnumerable[Int32])), Is.True);
    }

    public class Index
    {
        private int value;

        public int this[int index]
        {
            get => this.value == index ? 1 : 0;

            set => this.value = value == index ? 2 : 3;
        }
    }
}
