// <copyright file="BugTests.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace TestDynamicObjectRemoting;

public class BugTests : DomainTester
{
    [Test]
    public void Bug1ConsoleWriteLine()
        => Assert.DoesNotThrow(() => this.RemoteDomain.Root.System.Console.WriteLine("Hello"));
}
