# Dynamic Object Remoting

The library *Dynamic Object Remoting* allows one to access the domain of a remote .NET application using the C# `dynamic` keyword.
There is no need to have the remote libraries installed locally.

## Example
### Server
Create a test project, add the required package:
```shell
dotnet new console -n Server
cd Server
dotnet new nugetconfig
dotnet nuget add source -n gitlab --configfile nuget.config "https://gitlab.com/api/v4/projects/51335003/packages/nuget/index.json"
dotnet add package DynamicObjectRemoting.Server
```

Replace `Program.cs` with the following code, to create a listening server:
```csharp
using System.Net;
using DynamicObjectRemoting;

(Task listener, IPEndPoint endPoint) = await TcpService.ListenAsync("localhost", 0);
Console.WriteLine($"Listening on port {endPoint.Port}");
await listener;
```

Run the server (Ctrl+C to kill the program):
```shell
dotnet run
```

### Client
Create a test project, and add the required package:
```shell
dotnet new console -n Client
cd Client
dotnet new nugetconfig
dotnet nuget add source -n gitlab --configfile nuget.config "https://gitlab.com/api/v4/projects/51335003/packages/nuget/index.json"
dotnet add package DynamicObjectRemoting.Client
```

Replace `Program.cs` with the following code, to connect to the remote domain and to print `hello`:
```csharp
using DynamicObjectRemoting;

Console.Write("Enter listening port: ");
var port = int.Parse(Console.ReadLine());
using var remoteDomain = await TcpRemoteDomain.ConnectAsync("localhost", port);

var remoteConsole = remoteDomain.Root.System.Console;
remoteConsole.WriteLine("Hello");
```
