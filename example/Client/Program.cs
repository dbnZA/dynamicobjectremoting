﻿using DynamicObjectRemoting;

Console.Write("Enter listening port: ");
var port = int.Parse(Console.ReadLine());
using var remoteDomain = await TcpRemoteDomain.ConnectAsync("localhost", port);

var remoteConsole = remoteDomain.Root.System.Console;
remoteConsole.WriteLine("Hello");

