﻿using System.Net;
using DynamicObjectRemoting;

(Task listener, IPEndPoint endPoint) = await TcpService.ListenAsync("localhost", 0, CancellationToken.None);
Console.WriteLine($"Listening on port {endPoint.Port}");
await listener;

