// <copyright file="ProtocolException.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

/// <summary>
/// An exception indicating a badly formed message when communication between the client and the server.
/// </summary>
public class ProtocolException : Exception
{
    /// <summary>
    /// Initializes a new instance of the <see cref="ProtocolException"/> class.
    /// </summary>
    /// <param name="message">The message describing the protocol failure.</param>
    public ProtocolException(string message)
        : base(message)
    {
    }
}
