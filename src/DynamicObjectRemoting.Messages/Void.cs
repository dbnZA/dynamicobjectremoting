// <copyright file="Void.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

/// <summary>
/// A type indicating no return value.
/// </summary>
/// <remarks>
/// This is equivalent to F#'s unit type.
/// </remarks>
internal struct Void;
