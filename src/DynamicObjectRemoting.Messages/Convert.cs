// <copyright file="Convert.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

/// <summary>
/// Methods to convert objects to and from message encoding.
/// </summary>
internal static class Convert
{
    /// <summary>
    /// Convert the specified object into a message type/value pair.
    /// </summary>
    /// <param name="object">The object to encode.</param>
    /// <param name="objectToId">The function that will convert an arbitrary object into a unique ID.</param>
    /// <returns>The message <see cref="Argument"/> instance.</returns>
    public static Argument ToMessage(object? @object, Func<object, int> objectToId)
        => @object switch
        {
            Void => new Argument(),
            bool v => new Argument { Bool = v },
            byte v => new Argument { Byte = v },
            char v => new Argument { Char = v },
            decimal v => new Argument { Decimal = v.ToString("R") },
            double v => new Argument { Double = v },
            float v => new Argument { Float = v },
            int v => new Argument { Int32 = v },
            long v => new Argument { Int64 = v },
            nint v => new Argument { Nint = v },
            nuint v => new Argument { Nuint = v },
            null => new Argument { IsNull = true },
            sbyte v => new Argument { Sbyte = v },
            short v => new Argument { Int16 = v },
            string v => new Argument { String = v },
            uint v => new Argument { Uint32 = v },
            ulong v => new Argument { Uint64 = v },
            ushort v => new Argument { Uint16 = v },
            _ => new Argument { ObjectId = objectToId(@object) },
        };

    /// <summary>
    /// Convert the specified message type/value pair into an object.
    /// </summary>
    /// <param name="arg">The message <see cref="Argument"/> instance.</param>
    /// <param name="idToObject">A function that converts a unique ID into the object.</param>
    /// <returns>The decoded object.</returns>
    /// <exception cref="ProtocolException">Thrown when the message type/value pair contains incorrect
    /// values.</exception>
    public static object? ToObject(Argument arg, Func<int, object> idToObject)
        => arg.ValueCase switch
        {
            Argument.ValueOneofCase.Bool => arg.Bool,
            Argument.ValueOneofCase.Byte => (byte)arg.Byte,
            Argument.ValueOneofCase.Char => (char)arg.Char,
            Argument.ValueOneofCase.Decimal => decimal.Parse(arg.Decimal),
            Argument.ValueOneofCase.Double => arg.Double,
            Argument.ValueOneofCase.Float => arg.Float,
            Argument.ValueOneofCase.Int16 => (short)arg.Int16,
            Argument.ValueOneofCase.Int32 => arg.Int32,
            Argument.ValueOneofCase.Int64 => arg.Int64,
            Argument.ValueOneofCase.IsNull => null,
            Argument.ValueOneofCase.IsVoid => default(Void),
            Argument.ValueOneofCase.Nint => (nint)arg.Nint,
            Argument.ValueOneofCase.None => default(Void),
            Argument.ValueOneofCase.Nuint => (nuint)arg.Nuint,
            Argument.ValueOneofCase.ObjectId => idToObject(arg.ObjectId),
            Argument.ValueOneofCase.Sbyte => (sbyte)arg.Sbyte,
            Argument.ValueOneofCase.String => arg.String,
            Argument.ValueOneofCase.Uint16 => (ushort)arg.Uint16,
            Argument.ValueOneofCase.Uint32 => arg.Uint32,
            Argument.ValueOneofCase.Uint64 => arg.Uint64,
            var @case => throw new ProtocolException($"Unknown object type from server: {@case}"),
        };
}
