// <copyright file="Friends.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("DynamicObjectRemoting.Client")]
[assembly: InternalsVisibleTo("DynamicObjectRemoting.Server")]
