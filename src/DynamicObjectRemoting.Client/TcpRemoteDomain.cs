// <copyright file="TcpRemoteDomain.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

using System.Buffers;
using System.Net;
using System.Net.Sockets;
using Google.Protobuf;

/// <summary>
/// A remote namespace that communicates over TCP.
/// </summary>
public sealed class TcpRemoteDomain : IDisposable
{
    private readonly ArrayBufferWriter<byte> buffer = new();

    private readonly object syncLock = new();

    private Stream? stream;

    private TcpRemoteDomain(Stream? stream)
    {
        this.stream = stream;
        this.Root = this.Request(RequestType.GetRootObject) switch
        {
            DynamicRemoteObject obj => obj,
            var response => throw new ProtocolException($"Unexpected response from server: {response}"),
        };
    }

    /// <summary>
    /// Gets the root namespace of the remote domain.
    /// </summary>
    public dynamic Root { get; }

    internal bool IsDisposed => this.stream is null;

    private Stream Stream => this.stream ?? throw new ObjectDisposedException("Cannot use after disposed");

    /// <summary>
    /// Connect to a remove object server.
    /// </summary>
    /// <param name="endPoint">The end-point of the remote server.</param>
    /// <param name="ctx">The cancellation token to use.</param>
    /// <returns>A task representing the creation of a connection to the remove object server.</returns>
    public static async Task<TcpRemoteDomain> ConnectAsync(IPEndPoint endPoint, CancellationToken ctx = default)
    {
        Socket? client = null;
        try
        {
            client = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            await client.ConnectAsync(endPoint, ctx);

            var networkStream = new NetworkStream(client, ownsSocket: true);

            return new TcpRemoteDomain(networkStream);
        }
        catch
        {
            client?.Dispose();
            throw;
        }
    }

    /// <summary>
    /// Connect to a remove object server.
    /// </summary>
    /// <param name="host">The host IP or name of the remote object server.</param>
    /// <param name="port">The port on which to connect.</param>
    /// <param name="ctx">The cancellation token to use.</param>
    /// <returns>A task representing the creation of a connection to the remove object server.</returns>
    public static async Task<TcpRemoteDomain> ConnectAsync(string host, int port, CancellationToken ctx = default)
        => await ConnectAsync(new((await Dns.GetHostEntryAsync(host, ctx)).AddressList[0], port), ctx);

    /// <summary>
    /// Get the type of a type descriptor.
    /// </summary>
    /// <param name="obj">The remote proxy object.</param>
    /// <returns>The remote proxy object representing a <see cref="Type"/>.</returns>
    public dynamic TypeOf(dynamic obj)
        => this.Request(RequestType.TypeOf, obj) switch
        {
            DynamicRemoteObject @ref => @ref,
            _ => throw new ProtocolException("Unexpected response from server"),
        };

    /// <inheritdoc />
    public void Dispose()
    {
        if (this.stream is not null)
        {
            this.Request(RequestType.TerminateSession);
            this.stream.Dispose();
            this.stream = null;
        }
    }

    internal object? Request(RequestType request, params object?[] args)
    {
        Request message = new() { Request_ = request };
        message.Args.AddRange(args.Select(o => Convert.ToMessage(o, ObjectToId)));

        Response response;
        lock (this.syncLock)
        {
            this.buffer.Clear();
            message.WriteTo(this.buffer);
            this.Stream.Write(BitConverter.GetBytes(this.buffer.WrittenCount));
            this.Stream.Write(this.buffer.WrittenSpan);
            this.Stream.Flush();

            this.buffer.Clear();
            this.Stream.ReadExactly(this.buffer.GetSpan(sizeof(int))[..sizeof(int)]);
            var length = BitConverter.ToInt32(this.buffer.GetSpan()[..sizeof(int)]);
            this.Stream.ReadExactly(this.buffer.GetSpan(length)[..length]);
            response = Response.Parser.ParseFrom(this.buffer.GetSpan()[..length])
                ?? throw new ProtocolException("Empty response from server");
        }

        return response switch
        {
            { Status: ResponseType.Success } => Convert.ToObject(response.Return, IdToObject),
            { Status: ResponseType.NotFound } => default(NotFound),
            { Status: ResponseType.Exception, Return.ValueCase: Argument.ValueOneofCase.ObjectId } =>
                throw new RemoteDomainException(IdToObject(response.Return.ObjectId)),
            _ => throw new ProtocolException($"Unknown status from server: {response.Status}"),
        };

        object IdToObject(int id)
            => new DynamicRemoteObject(this, id);

        int ObjectToId(object @object)
            => @object switch
            {
                DynamicRemoteObject obj when obj.RemoteDomain == this => obj.ObjectId,
                _ => throw new ArgumentException($"Cannot pass type to server: {@object.GetType().Name}"),
            };
    }
}
