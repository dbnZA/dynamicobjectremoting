// <copyright file="RemoteDomainException.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

/// <summary>
/// Represents an exception raised in the remote domain.
/// </summary>
/// <param name="remoteException">A remote object representing the exception raised.</param>
public class RemoteDomainException(dynamic remoteException) : Exception
{
    /// <summary>
    /// Gets a dynamic object that represents the remote object.
    /// </summary>
    public dynamic RemoteException => remoteException;

    /// <inheritdoc />
    public override string Message
    {
        get
        {
            try
            {
                return remoteException.Message;
            }
            catch (Exception ex) when (ex is not RemoteDomainException)
            {
                return $"Cannot get remote exception message: {ex.Message}";
            }
        }
    }
}
