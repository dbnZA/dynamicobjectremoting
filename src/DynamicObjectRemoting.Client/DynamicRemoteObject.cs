﻿// <copyright file="DynamicRemoteObject.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

using System.Dynamic;
using Microsoft.CSharp.RuntimeBinder;

internal class DynamicRemoteObject(TcpRemoteDomain remoteDomain, int objectId) : DynamicObject
{
    ~DynamicRemoteObject()
    {
        if (!this.RemoteDomain.IsDisposed)
        {
            this.RemoteDomain.Request(RequestType.ReleaseObject, this);
        }
    }

    internal int ObjectId { get; } = objectId;

    internal TcpRemoteDomain RemoteDomain { get; } = remoteDomain;

    public override bool TryInvoke(InvokeBinder binder, object?[]? args, out object? result)
        => (result = this.RemoteDomain.Request(RequestType.Invoke, args!.Prepend(this).ToArray())) is not NotFound;

    public override bool TryGetMember(GetMemberBinder binder, out object? result)
        => (result = this.RemoteDomain.Request(RequestType.GetMember, this, binder.Name)) switch
        {
            NotFound => throw new RuntimeBinderException($"No definition for member '{binder.Name}'"),
            _ => true,
        };

    public override bool TrySetMember(SetMemberBinder binder, object? value)
        => this.RemoteDomain.Request(RequestType.SetMember, this, binder.Name, value) switch
        {
            NotFound => throw new RuntimeBinderException($"No definition for setting member '{binder.Name}'"),
            _ => true,
        };

    public override bool TryGetIndex(GetIndexBinder binder, object[] indexes, out object? result)
        => (result = this.RemoteDomain.Request(RequestType.GetIndex, indexes.Prepend(this).ToArray())) is not NotFound;

    public override bool TrySetIndex(SetIndexBinder binder, object[] indexes, object? value)
        => this.RemoteDomain.Request(RequestType.SetIndex, indexes.Prepend(value).Prepend(this).ToArray()) is not NotFound;
}
