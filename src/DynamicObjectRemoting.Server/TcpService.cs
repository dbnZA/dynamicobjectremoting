// <copyright file="TcpService.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

using System.Net;
using System.Net.Sockets;

/// <summary>
/// A server that communicates over TCP.
/// </summary>
public static class TcpService
{
    /// <summary>
    /// Listen for and process any incoming connections.
    /// </summary>
    /// <remarks>
    /// On cancellation all active sessions are terminated.  This method does not return unless cancelled.
    /// </remarks>
    /// <param name="endPoint">The end-point from which to listen.</param>
    /// <param name="ctx">The cancellation token that will cause this method to return.</param>
    /// <returns>A task representing the listening service, and the end-point listening for incoming connections.
    /// </returns>
    public static (Task ConnectionTask, IPEndPoint EndPoint) ListenAsync(
        IPEndPoint endPoint,
        CancellationToken ctx = default)
    {
        var types = AppDomain.CurrentDomain
            .GetAssemblies()
            .Where(a => !a.IsDynamic)
            .SelectMany(a => a.GetExportedTypes())
            .Where(t => t is { IsNested: false, Namespace: not null })
            .Select(t => (Namespace: t.Namespace!.Split('.'), t))
            .Partition(0);

        Socket? listener = null;
        try
        {
            listener = new Socket(endPoint.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            listener.Bind(endPoint);
            listener.Listen(100);

            return (ListenAsync(types, listener, ctx), (IPEndPoint)listener.LocalEndPoint!);
        }
        catch
        {
            listener?.Dispose();
            throw;
        }
    }

    /// <summary>
    /// Listen for and process any incoming connections.
    /// </summary>
    /// <remarks>
    /// On cancellation all active sessions are terminated.  This method does not return unless cancelled.
    /// </remarks>
    /// <param name="host">The host name or IP address to listen from.</param>
    /// <param name="port">The port to listen from.</param>
    /// <param name="ctx">The cancellation token that will cause this method to return.</param>
    /// <returns>A task, that returns another task representing the listening service, that completes once listening
    /// has started, and the end-point listening for incoming connections.</returns>
    public static async Task<(Task ConnectionTask, IPEndPoint EndPoint)> ListenAsync(
        string host,
        int port,
        CancellationToken ctx = default)
        => ListenAsync(new IPEndPoint((await Dns.GetHostEntryAsync(host, ctx)).AddressList[0], port), ctx);

    internal static Dictionary<string, (string[] Namespace, Type Type)[]> Partition(
        this IEnumerable<(string[] Namespace, Type Type)> types,
        int depth)
        => types
            .GroupBy(x => x.Namespace.Length == depth
                ? x.Type.Name.Split("`", 2)[0]
                : x.Namespace[depth])
            .ToDictionary(g => g.Key, g => g.ToArray());

    private static async Task ListenAsync(
        Dictionary<string, (string[] Namespace, Type Type)[]> types,
        Socket listener,
        CancellationToken ctx)
    {
        List<WeakReference<TcpConnection>> connections = new();
        try
        {
            while (!ctx.IsCancellationRequested)
            {
                Socket handler = await listener.AcceptAsync(ctx);
                var connection = new WeakReference<TcpConnection>(new TcpConnection(types, handler, ctx));
                var savedHandler = false;
                for (var i = 0; i < connections.Count; i++)
                {
                    if (!connections[i].TryGetTarget(out _))
                    {
                        connections[i] = connection;
                        savedHandler = true;
                        break;
                    }
                }

                if (!savedHandler)
                {
                    connections.Add(connection);
                }
            }
        }
        catch (OperationCanceledException)
        {
        }
        finally
        {
            try
            {
                await Task.WhenAll(connections
                    .Select(w => w.TryGetTarget(out TcpConnection? connection)
                        ? connection.DisposeAsync().AsTask()
                        : Task.CompletedTask));
            }
            finally
            {
                listener.Dispose();
            }
        }
    }
}
