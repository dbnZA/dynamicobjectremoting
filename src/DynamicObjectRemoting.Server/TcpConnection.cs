// <copyright file="TcpConnection.cs" company="Dynamic Object Remoting Project">
// Copyright (c) Dynamic Object Remoting Project. All rights reserved.
// </copyright>
namespace DynamicObjectRemoting;

using System.Buffers;
using System.Net.Sockets;
using System.Reflection;
using Google.Protobuf;

internal sealed class TcpConnection : IAsyncDisposable
{
    private static readonly (object?, ResponseType) NotFound = (default(Void), ResponseType.NotFound);

    private static readonly (object?, ResponseType) BadRequest = (default(Void), ResponseType.BadRequest);

    private readonly Dictionary<object, int> objectToId = new(ReferenceEqualityComparer.Instance);

    private readonly List<Ref?> idToObject = new();

    private readonly Dictionary<string, (string[] Namespace, Type Type)[]> types;

    private readonly CancellationTokenSource tokenSource;

    private readonly Task task;

    public TcpConnection(Dictionary<string, (string[] Namespace, Type Type)[]> types, Socket handler, CancellationToken ctx)
    {
        this.types = types;
        this.tokenSource = CancellationTokenSource.CreateLinkedTokenSource(ctx);
        this.task = this.HandleAsync(handler, this.tokenSource.Token);
    }

    ~TcpConnection()
        => this.tokenSource.Dispose();

    public async ValueTask DisposeAsync()
    {
        this.tokenSource.Cancel();
        await this.task;
    }

    private async Task HandleAsync(Socket handler, CancellationToken ctx)
    {
        await using NetworkStream networkStream = new(handler, ownsSocket: true);
        ArrayBufferWriter<byte> buffer = new();

        Request? message;
        do
        {
            buffer.Clear();
            await networkStream.ReadExactlyAsync(buffer.GetMemory(sizeof(int))[..sizeof(int)], ctx);
            var length = BitConverter.ToInt32(buffer.GetSpan()[..sizeof(int)]);
            await networkStream.ReadExactlyAsync(buffer.GetMemory(length)[..length], ctx);
            message = Request.Parser.ParseFrom(buffer.GetSpan()[..length]);

            ResponseType status;
            object? responseArg;
            try
            {
                (responseArg, status) = message?.Request_ switch
                {
                    RequestType.GetRootObject => this.CreateObject(message),
                    RequestType.ReleaseObject => this.ResponseObject(message),
                    RequestType.TypeOf => this.TypeOf(message),
                    RequestType.Invoke => this.Invoke(message),
                    RequestType.GetMember => this.GetMember(message),
                    RequestType.SetMember => this.SetMember(message),
                    RequestType.GetIndex => this.GetIndex(message),
                    RequestType.SetIndex => this.SetIndex(message),
                    RequestType.TerminateSession => (default(Void), ResponseType.Success),
                    _ => BadRequest,
                };
            }
            catch (Exception e)
            {
                responseArg = e.InnerException ?? e;
                status = ResponseType.Exception;
            }

            Response response = new() { Status = status, Return = Convert.ToMessage(responseArg, this.ObjectToId) };

            buffer.Clear();
            response.WriteTo(buffer);
            await networkStream.WriteAsync(BitConverter.GetBytes(buffer.WrittenCount), ctx);
            await networkStream.WriteAsync(buffer.WrittenMemory, ctx);
            await networkStream.FlushAsync(ctx);
        }
        while (message?.Request_ != RequestType.TerminateSession);

        this.idToObject.Clear();
        this.objectToId.Clear();
    }

    private (object? ResponseArg, ResponseType Status) TypeOf(Request message)
        => this.GetArguments(message, referenceArguments: 1) switch
        {
            [RefTypes { Types: [var type] }] => (type, ResponseType.Success),
            _ => BadRequest,
        };

    private (object? ResponseArg, ResponseType Status) SetIndex(Request message)
    {
        return this.GetArguments(message, referenceArguments: 1) switch
        {
            [RefObject { Arg: var obj }, var value, _, ..] args
                => SetIndex(obj.GetType(), obj, args[2..], value),
            [RefTypes { Types: var types }, var value, _, ..] args =>
                types.SingleOrDefault(t => !t.IsGenericTypeDefinition) is { } type
                    ? SetIndex(type, null, args[2..], value)
                    : BadRequest,
            _ => BadRequest,
        };

        (object?, ResponseType) SetIndex(Type type, object? obj, object?[] indexes, object? value)
            => type.GetProperty("Item") is { SetMethod: { } setter } property
                    && setter.GetParameters().Length > 1
                ? SetValue(property, obj, indexes, value)
                : NotFound;

        (object?, ResponseType) SetValue(PropertyInfo property, object? obj, object?[] indexes, object? value)
        {
            property.SetValue(obj, value, indexes);
            return (default(Void), ResponseType.Success);
        }
    }

    private (object? ResponseArg, ResponseType Status) GetIndex(Request message)
    {
        return (this.GetArguments(message, referenceArguments: int.MaxValue)
                ?? this.GetArguments(message, referenceArguments: 1)) switch
            {
                [RefObject { Arg: var obj }, _, ..] args => GetIndex(obj.GetType(), obj, args[1..]),
                [RefTypes typeRef, _, ..] args when args.Skip(1).All(a => a is RefTypes { Types: [{ IsGenericTypeDefinition: false }] }) =>
                    GetGenericType(typeRef, args[1..].Select(a => ((RefTypes)a!).Types[0]).ToArray()),
                [RefMethods methodRef, _, ..] args when args.Skip(1).All(a => a is RefTypes { Types: [{ IsGenericTypeDefinition: false }] }) =>
                    GetGenericMethod(methodRef, args[1..].Select(a => ((RefTypes)a!).Types[0]).ToArray()),
                [RefTypes { Types: var types }, _, ..] args =>
                    types.SingleOrDefault(t => !t.IsGenericTypeDefinition) is { } type
                        ? GetIndex(type, null, args[1..])
                        : BadRequest,
                _ => BadRequest,
            };

        (object?, ResponseType) GetIndex(Type type, object? obj, object?[] indexes)
            => type.GetProperty("Item") switch
            {
                { GetMethod: { } getter } property when getter.GetParameters().Length > 0
                    => (property.GetValue(obj, indexes), ResponseType.Success),
                _ => NotFound,
            };

        (object?, ResponseType) GetGenericType(RefTypes typeRef, Type[] args)
        {
            var type = typeRef.Types.SingleOrDefault(t => args.Length == t.GetGenericArguments().Length);
            return type is not null
                ? (this.AddRef(id => new RefTypes(id, new[] { type.MakeGenericType(args) })), ResponseType.Success)
                : NotFound;
        }

        (object?, ResponseType) GetGenericMethod(RefMethods methodRef, Type[] args)
        {
            var method = methodRef.Methods.SingleOrDefault(t => args.Length == t.GetGenericArguments().Length);
            return method is not null
                ? (this.AddRef(id => new RefMethods(id, methodRef.Arg, new[] { method.MakeGenericMethod(args) })), ResponseType.Success)
                : NotFound;
        }
    }

    private (object? ResponseArg, ResponseType Status) Invoke(Request message)
    {
        return this.GetArguments(message, referenceArguments: 1) switch
        {
            [RefMethods { Methods: var methods, Arg: var arg }, ..] args =>
                methods.Where(m => !m.IsGenericMethodDefinition && ParametersMatch(m, args)).ToArray() switch
                    {
                        [_, _, ..] methodInfos => InvokeMethod(methodInfos.Where(m => ExactParametersMatch(m, args)).ToArray(), arg, args),
                        var methodInfos => InvokeMethod(methodInfos, arg, args),
                    },
            [RefTypes { Types: var types }, ..] args =>
                types.SingleOrDefault(t => !t.IsGenericTypeDefinition) is { } type
                && type.GetConstructors().SingleOrDefault(c => ParametersMatch(c, args)) is { } ctor
                    ? (ctor.Invoke(args[1..]), ResponseType.Success)
                    : NotFound,
            _ => BadRequest,
        };

        static bool ParametersMatch(MethodBase methodInfo, object?[] args)
        {
            var parameters = methodInfo.GetParameters();
            if (args.Length != parameters.Length + 1)
            {
                return false;
            }

            return !parameters
                .Where((t, i) => !t.ParameterType.IsInstanceOfType(args[i + 1]))
                .Any();
        }

        static bool ExactParametersMatch(MethodBase methodInfo, object?[] args)
            => !methodInfo
                .GetParameters()
                .Where((t, i) => t.ParameterType != args[i + 1]?.GetType())
                .Any();

        (object? ResponseArg, ResponseType Status) InvokeMethod(MethodInfo[] methodInfos, object? arg, object?[] args)
            => methodInfos switch
            {
                [var methodInfo] => (methodInfo.Invoke(arg, args[1..]), ResponseType.Success),
                _ => NotFound,
            };
    }

    private (object? ResponseArg, ResponseType Status) SetMember(Request message)
    {
        return this.GetArguments(message, referenceArguments: 1) switch
        {
            [RefObject { Arg: var obj }, string name, var arg]
                => SetMember(obj.GetType(), obj, name, arg),
            [RefTypes { Types: var types }, string name, var arg] args =>
                types.SingleOrDefault(t => !t.IsGenericTypeDefinition) is { } type
                    ? SetMember(type, null, name, arg)
                    : BadRequest,
            _ => BadRequest,
        };

        (object?, ResponseType) SetMember(Type type, object? obj, string name, object? value)
            => type.GetProperty(name) is { SetMethod: { } setter } property
                    && setter.GetParameters().Length == 1
                ? SetValue(property, obj, value)
                : NotFound;

        (object? ResponseArg, ResponseType Status) SetValue(PropertyInfo property, object? obj, object? value)
        {
            property.SetValue(obj, value);
            return (default(Void), ResponseType.Success);
        }
    }

    private (object? ResponseArg, ResponseType Status) GetMember(Request message)
    {
        return this.GetArguments(message, referenceArguments: 1) switch
        {
            [RefObject { Arg: var obj }, string name] => GetTypeMember(obj.GetType(), obj, name),
            [RefTypes { Types: var types }, string name] =>
                types.SingleOrDefault(t => !t.IsGenericTypeDefinition) is { } type
                    ? GetTypeMember(type, null, name)
                    : BadRequest,
            [RefNamespace { Types: var types, Depth: var depth }, string name] =>
                types.TryGetValue(name, out var innerTypes)
                    ? (
                        innerTypes.All(t => t.Namespace.Length == depth)
                            ? this.AddRef(id => new RefTypes(id, innerTypes.Select(t => t.Type).ToArray()))
                            : this.AddRef(id => new RefNamespace(id, innerTypes.Partition(depth + 1), depth + 1)),
                        ResponseType.Success)
                    : NotFound,
            _ => BadRequest,
        };

        (object? ResponseArg, ResponseType Status) GetTypeMember(Type type, object? obj, string name)
            => type.GetMembers()
                   .Where(m => m.Name == name)
                   .ToArray() switch
                {
                    [PropertyInfo { GetMethod: { } getter } property] when getter.GetParameters().Length == 0
                        => (property.GetValue(obj), ResponseType.Success),
                    var members when members.All(m => m is MethodInfo) && members.Length > 0
                        => (this.AddRef(id => new RefMethods(id, obj, Array.ConvertAll(members, m => (MethodInfo)m))),
                            ResponseType.Success),
                    var members when members.All(m => m is Type) && members.Length > 0
                        => (this.AddRef(id => new RefTypes(id, Array.ConvertAll(members, m => (Type)m))),
                            ResponseType.Success),
                    _ => NotFound,
                };
    }

    private (object? ResponseArg, ResponseType Status) ResponseObject(Request message)
    {
        if (this.GetArguments(message, referenceArguments: 1) is [Ref { ObjectId: var objectId } @ref])
        {
            if (@ref is RefObject { Arg: var arg })
            {
                this.objectToId.Remove(arg);

                // ReSharper disable once RedundantAssignment
                arg = null;
            }

            this.idToObject[objectId] = null;
            return (default(Void), ResponseType.Success);
        }

        return BadRequest;
    }

    private (object? ResponseArg, ResponseType Status) CreateObject(Request message)
        => this.GetArguments(message, referenceArguments: 0) is []
            ? (this.AddRef(id => new RefNamespace(id, this.types, 0)), ResponseType.Success)
            : BadRequest;

    private int ObjectToId(object arg)
    {
        if (arg is Ref @ref)
        {
            return @ref.ObjectId;
        }

        if (!this.objectToId.TryGetValue(arg, out var objectId))
        {
            this.objectToId.Add(arg, objectId = this.AddRef(id => new RefObject(id, arg)).ObjectId);
        }

        return objectId;
    }

    private object?[]? GetArguments(Request message, int referenceArguments)
    {
        var missingObject = false;
        var args = message.Args
            .Select((v, i) =>
            {
                var isReference = false;
                var arg = Convert.ToObject(v, IdToObject);
                if (i < referenceArguments && !isReference)
                {
                    missingObject = true;
                    return true;
                }

                return arg;

                object IdToObject(int id)
                {
                    if (id >= 0 && id < this.idToObject.Count)
                    {
                        Ref? obj = this.idToObject[id];
                        if (obj is not null)
                        {
                            if (i < referenceArguments)
                            {
                                isReference = true;
                                return obj;
                            }

                            if (obj is RefObject { Arg: var value })
                            {
                                return value;
                            }
                        }
                    }

                    missingObject = true;
                    return null!;
                }
            })
            .ToArray();

        return missingObject ? null : args;
    }

    private Ref AddRef(Func<int, Ref> factory)
    {
        Ref @ref = factory(this.idToObject.Count);
        this.idToObject.Add(@ref);
        return @ref;
    }

    private abstract record Ref(int ObjectId);

    private record RefNamespace(
        int ObjectId,
        Dictionary<string, (string[] Namespace, Type Type)[]> Types,
        int Depth) : Ref(ObjectId);

    private record RefTypes(int ObjectId, Type[] Types) : Ref(ObjectId);

    private sealed record RefObject(int ObjectId, object Arg) : Ref(ObjectId);

    private sealed record RefMethods(int ObjectId, object? Arg, MethodInfo[] Methods) : Ref(ObjectId);
}
